require 'csv'
require 'date'

input = CSV.read("input2.csv", headers: true)

puts "Eligible:"
eligible =
    input
        .select do |person|
            (person["Do you prefer in-person or remotely?"] == "remotely") &&
                Date.parse(person["When did you join Remote?"]) < Date.today - 180
        end
        .each do |person|
            puts person["Email Address"]
            person["Days at Remote"] = Date.today.mjd - Date.parse(person["When did you join Remote?"]).mjd
        end

tracking_nr = 0

puts " "
# Hand out tickets
eligible.each do |person|
    person["TICKETS"] = tracking_nr + person["Days at Remote"]
    tracking_nr =  person["TICKETS"] + 1
end

# Roll the dice!
# rand is exclusive to the max number, so while tracking_nr is + 1 of the max
# number that's what we want so we end up with 0..max tracking number
ball = rand(tracking_nr)

puts "THE BALL is #{ball}"

# Find the winner
winner = eligible.find { |person| person["TICKETS"] >= ball }

puts "WINNER"
puts winner["Email Address"]
puts "WINNER"
